# ThreeFive #

[![CircleCI](https://circleci.com/bb/scurri/threefive.svg?style=svg)](https://circleci.com/bb/scurri/threefive)
[![codecov](https://codecov.io/bb/scurri/threefive/branch/master/graph/badge.svg)](https://codecov.io/bb/scurri/threefive)

ThreeFive it is a simple project make with python for printing the numbers from 1 to 100. But for multiples of three print “Three” instead of the number and for the multiples of five print “Five”. For numbers which are multiples of both three and five print “ThreeFive”.



## Dependencies

- [Python 3.6](https://www.python.org/downloads/)

## Set up

1.  Create a virtualenv with python 3.6:

    ```
    virtualenv -p python3 .venv
    ```

2. Install the dependencies of project:

    ```
    make requirements-dev
    ```


## Run the project

1. Run the command:

    ```
    make run
    ```

## Run the tests

1. Run the command:

    ```
    make test
    ```

## Deploy to production

* This project have a dockerfile to help you to deploy the project in all kinds of cloud also have new relic configured to monitoring.

* For deploy in production you just need create a docker image version of the project and push to your infrastructure and set 3 variables in the environment.


    
    DEPLOY_ENV = "Production"
    
    NEW_RELIC_LICENSE_KEY = "YOUR_LICENSE_HERE"
    
    NEW_RELIC_LOG = "stdout"
    