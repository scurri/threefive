FROM python:3.6.4-alpine
MAINTAINER Vitor Mantovani <vtrmantovani@gmail.com>

RUN apk add --no-cache --update bash git openssh mariadb-dev libffi-dev linux-headers alpine-sdk

RUN addgroup threefive && adduser -D -h /home/threefive -G threefive threefive

USER threefive
RUN mkdir /home/threefive/threefive
RUN mkdir /home/threefive/logs
ADD wsgi.py /home/threefive/
ADD requirements.txt /home/threefive/
ADD threefive /home/threefive/threefive

RUN cd /home/threefive && rm -rf /home/threefive/.venv && /usr/local/bin/python -m venv .venv \
    && /home/threefive/.venv/bin/pip install --upgrade pip
RUN cd /home/threefive && /home/threefive/.venv/bin/pip install -r requirements.txt

USER root
RUN chown threefive.threefive /home/threefive -R

USER threefive
ADD ./dockerfiles/uwsgi.ini /home/threefive/

ADD ./dockerfiles/newrelic.ini /home/threefive/
ENV NEW_RELIC_CONFIG_FILE=/home/threefive/newrelic.ini

EXPOSE 8080
CMD ["/home/threefive/.venv/bin/newrelic-admin", "run-program", "/home/threefive/.venv/bin/uwsgi", "--ini", "/home/threefive/uwsgi.ini"]