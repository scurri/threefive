from tests.base import BaseTestCase
from threefive.business import (is_multiple_of_five, is_multiple_of_three,
                                is_multiple_of_three_and_five)


class TestBusinessCase(BaseTestCase):

    def setUp(self):
        super(TestBusinessCase, self).setUp()

    def test_is_multiple_of_three(self):
        result = is_multiple_of_three(3)
        self.assertTrue(result)

    def test_not_is_multiple_of_three(self):
        result = is_multiple_of_three(4)
        self.assertFalse(result)

    def test_is_multiple_of_three_with_not_integer_number(self):
        with self.assertRaisesRegexp(TypeError, 'The value need be a integer'):
            is_multiple_of_three("3")

    def test_is_multiple_of_five(self):
        result = is_multiple_of_five(10)
        self.assertTrue(result)

    def test_not_is_multiple_of_five(self):
        result = is_multiple_of_five(11)
        self.assertFalse(result)

    def test_is_multiple_of_five_with_not_integer_number(self):
        with self.assertRaisesRegexp(TypeError, 'The value need be a integer'):
            is_multiple_of_five("10")

    def test_is_multiple_of_three_and_five(self):
        result = is_multiple_of_three_and_five(15)
        self.assertTrue(result)

    def test_not_is_multiple_of_three_and_five(self):
        result = is_multiple_of_three_and_five(16)
        self.assertFalse(result)

    def test_is_multiple_of_three_and_five_with_not_integer_number(self):
        with self.assertRaisesRegexp(TypeError, 'The value need be a integer'):
            is_multiple_of_three_and_five("15")
