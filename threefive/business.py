
def _is_int(value):
    if type(value) is int:
        return True
    raise TypeError('The value need be a integer')


def is_multiple_of_three(value):
    try:
        if _is_int(value) and value % 3 == 0:
            return True

        return False
    except TypeError as e:
        raise TypeError(e)


def is_multiple_of_five(value):
    try:
        if _is_int(value) and value % 5 == 0:
            return True

        return False
    except TypeError as e:
        raise TypeError(e)


def is_multiple_of_three_and_five(value):
    try:
        if _is_int(value) and value % 3 == 0 and value % 5 == 0:
            return True

        return False
    except TypeError as e:
        raise TypeError(e)
