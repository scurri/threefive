from flask import Blueprint, jsonify

from threefive.business import (is_multiple_of_five, is_multiple_of_three,
                                is_multiple_of_three_and_five)

common = Blueprint('common', __name__)


@common.route('/', methods=['GET'])
def home():
    list_of_numbers = []
    for i in range(1, 101):
        if is_multiple_of_three(i):
            if is_multiple_of_three_and_five(i):
                list_of_numbers.append('Threefive')
            else:
                list_of_numbers.append('Three')

        elif is_multiple_of_five(i):
            if is_multiple_of_three_and_five(i):
                list_of_numbers.append('Threefive')
            else:
                list_of_numbers.append('Five')
        else:
            list_of_numbers.append(str(i))

    return jsonify({"service": "Threefive Challenge",
                    "version": "1.0",
                    "numbers": list_of_numbers})
