import logging
import os
import sys

from flask import Flask

logger = logging.getLogger(__name__)


def create_app(config_var=os.getenv('DEPLOY_ENV', 'Development')):
    app = Flask(__name__)
    app.config.from_object('threefive.config.%sConfig' % config_var)
    app.config['DEPLOY_ENV'] = config_var

    configure_logger(app)

    register_blueprints(app)

    return app


def configure_logger(app):
    if not logger.handlers:
        ch = logging.StreamHandler(sys.stdout)
        ch.setLevel(app.config['LOGS_LEVEL'])
        logger.addHandler(ch)


def register_blueprints(app):
    from threefive.views.common import common
    app.register_blueprint(common)
