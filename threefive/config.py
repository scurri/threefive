import logging


class BaseConfig(object):
    FLASK_DEBUG = False
    TESTING = False

    LOGS_LEVEL = logging.INFO
    FLASK_ENV = ''


class DevelopmentConfig(BaseConfig):
    FLASK_DEBUG = True
    LOGS_LEVEL = logging.DEBUG


class TestingConfig(BaseConfig):
    DEBUG = True
    TESTING = True


class ProductionConfig(BaseConfig):
    pass
